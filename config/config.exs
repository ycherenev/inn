# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :inn,
  ecto_repos: [Inn.Repo]

# Configures the endpoint
config :inn, InnWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "STfRf3IjWJ2Td1u8dgSzx7bc+u7yQZXwuQ5INtiVL6fUAxTdX7SfP3svYabw0hS3",
  render_errors: [view: InnWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Inn.PubSub, adapter: Phoenix.PubSub.PG2]

config :inn, Inn.Scheduler,
  jobs: [
    # Every minute
    {"* * * * *", {Inn.RedisUtils, :clear_expiered_ips, []}}
  ]

# Configures Guardian
config :inn, Inn.Auth.Guardian,
  issuer: "inn",
  secret_key: "HNinpKh9Ne3tr8BpjCpAEh0xzCqTIG3PWsfkR2AtzvUaRIpbs6oIQ9RcmjmGPekJ"

config :inn, Inn.Auth.AuthAccessPipeline,
  module: Inn.Auth.Guardian,
  error_handler: Inn.Auth.AuthErrorHandler

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

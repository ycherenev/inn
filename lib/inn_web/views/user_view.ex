defmodule InnWeb.UserView do
  use InnWeb, :view
  import InnWeb.Router.Helpers

  def render("user.json", %{user: user}) do
    %{id: user.id, username: user.name}
  end
end

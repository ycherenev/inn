defmodule InnWeb.InnChannel do
  use Phoenix.Channel

  alias Inn.RedisUtils
  alias Inn.Core.InnService

  require Logger

  def join(_name, _params, socket) do
    inn_list = InnService.get_list()

    Logger.info("join user with ip [#{socket.assigns.user_ip}]")
    {:ok, %{inn_list: inn_list}, socket}
  end

  def handle_in(_name, %{"inn_value" => inn_value}, socket) do
    with false <- RedisUtils.ip_in_blocked_list?(socket.assigns.user_ip),
         {:ok, inn} <- InnService.create_new(inn_value, socket.assigns.user_ip) do
      broadcast!(socket, "inn:checked", %{inn: inn})

      {:reply, :ok, socket}
    else
      {:error, errors} ->
        {:reply, {:error, errors}, socket}

      true ->
        {:reply,
         {:error, %{reason: "Проверка невозможна, т.к. ваш IP адрес находится в черном списке"}},
         socket}
    end
  end
end

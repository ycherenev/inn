defmodule InnWeb.OperatorController do
  require Logger

  use InnWeb, :controller
  import InnWeb.Router.Helpers
  import Inn.Auth, only: [load_current_user: 2]

  alias Inn.Policies.Users
  alias Inn.RedisUtils
  alias Inn.Repo
  alias Inn.Core.Inn
  plug(:load_current_user)

  def index(conn, _params) do
    inn_list = Repo.all(Inn)
    render(conn, "index.html", inn_list: inn_list)
  end

  def delete(conn, %{"id" => inn_id}) do
    Repo.get!(Inn, inn_id) |> Repo.delete!()

    conn
    |> put_flash(:info, "Inn Deleted")
    |> redirect(to: operator_path(conn, :index))
  end

  @spec block_ip(Plug.Conn.t(), map) :: Plug.Conn.t()
  def block_ip(conn, %{"ip" => ip, "seconds" => seconds}) do
    with :ok <-
           Bodyguard.permit(
             Users,
             :block_ip,
             Guardian.Plug.current_resource(conn),
             %{}
           ) do
      time =
        Timex.now()
        |> Timex.shift(seconds: String.to_integer(seconds))
        |> Timex.to_unix()

      RedisUtils.add_ip_to_block_list(ip, time)
      Logger.info("ip: [#{ip}] blocked! for #{seconds} seconds")

      conn
      |> put_flash(:info, "ip: #{ip} blocked!")
      |> redirect(to: operator_path(conn, :index))
    end
  end
end

defmodule InnWeb.UserController do
  use InnWeb, :controller
  import InnWeb.Router.Helpers

  alias Inn.Accounts
  alias Inn.Accounts.User

  def new(conn, _params) do
    changeset = Accounts.change_user_registration(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        conn
        |> Inn.Auth.login(user)
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: operator_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end

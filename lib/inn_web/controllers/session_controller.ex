defmodule InnWeb.SessionController do
  import InnWeb.Router.Helpers
  use InnWeb, :controller

  def new(conn, _) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"username" => user, "password" => password}}) do
    case Inn.Auth.authenticate_user(user, password) do
      {:ok, user} ->
        conn
        |> Inn.Auth.login(user)
        |> put_flash(:info, "Welcome back, #{user.username}!")
        |> redirect(to: operator_path(conn, :index))

      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> Inn.Auth.logout()
    |> render("new.html")
  end
end

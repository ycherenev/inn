defmodule InnWeb.AuthController do
  use InnWeb, :controller

  def token(conn, _params) do
    user_ip =
      conn.remote_ip
      |> Tuple.to_list()
      |> Enum.join(".")

    token = Phoenix.Token.sign(InnWeb.Endpoint, "user salt", user_ip)
    json(conn, %{token: token})
  end
end

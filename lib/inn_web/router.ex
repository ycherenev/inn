defmodule InnWeb.Router do
  use InnWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "http://localhost:3000"
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug(Inn.Auth.AuthAccessPipeline)
  end

  scope "/", InnWeb do
    pipe_through :browser

    get "/", SessionController, :new
    resources "/users", UserController, only: [:new, :create]
    resources "/sessions", SessionController, only: [:new, :create]
  end

  scope "/operator", InnWeb do
    pipe_through [:browser, :auth]

    get "/", OperatorController, :index
    resources "/sessions", SessionController, only: [:delete]
    delete "/inn/:id", OperatorController, :delete
    post "/block-ip", OperatorController, :block_ip
  end

  scope "/auth", InnWeb do
    pipe_through [:api]

    get "/token", AuthController, :token
  end

  # Other scopes may use custom stacks.
  # scope "/api", InnWeb do
  #   pipe_through :api
  # end
end

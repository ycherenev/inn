defmodule Inn.RedisUtils do
  require Logger

  def add_ip_to_block_list(ip, time) do
    Redix.command(
      :redix,
      ["ZADD", "ip_block_list", time, ip]
    )
  end

  def ip_in_blocked_list?(ip) do
    get_blocked_ip_list()
    |> Enum.member?(ip)
  end

  def get_blocked_ip_list() do
    {:ok, ip_list} = Redix.command(:redix, ["ZRANGEBYSCORE", "ip_block_list", "-inf", "+inf"])

    ip_list
  end

  def clear_expiered_ips() do
    time = Timex.to_unix(Timex.now())

    {:ok, ip_list} =
      Redix.command(
        :redix,
        ["ZREMRANGEBYSCORE", "ip_block_list", "-inf", "(#{time}"]
      )

    Logger.info("clear expiered ips #{ip_list}")
  end
end

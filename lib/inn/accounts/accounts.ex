defmodule Inn.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false

  alias Inn.Repo
  alias Inn.Accounts.Role
  alias Inn.Accounts.User

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def get_user(id) do
    User
    |> Repo.get(id)
    |> Repo.preload(roles: from(r in Role, select: r.name))
    |> is_admin()
  end

  defp is_admin(%User{roles: roles} = user) do
    %{user | is_admin: Enum.member?(roles, "Admin")}
  end

  def change_user_registration(%User{} = user) do
    User.registration_changeset(user, %{})
  end
end

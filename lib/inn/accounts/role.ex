defmodule Inn.Accounts.Role do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias Inn.Accounts.Role
  alias Inn.Accounts.User

  schema "roles" do
    field :name, :string

    many_to_many :users, User,
      join_through: "users_roles",
      on_delete: :delete_all,
      on_replace: :delete

    timestamps()
  end

  def changeset(%Role{} = role, attrs \\ %{}) do
    role
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end

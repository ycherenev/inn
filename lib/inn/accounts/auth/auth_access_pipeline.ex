defmodule Inn.Auth.AuthAccessPipeline do
  use Guardian.Plug.Pipeline, otp_app: :inn

  plug(Guardian.Plug.VerifySession, claims: %{"typ" => "access"})
  plug(Guardian.Plug.EnsureAuthenticated)
  plug(Guardian.Plug.LoadResource)
end

defmodule Inn.Accounts.User do
  use Ecto.Schema

  import Ecto.Changeset

  alias Inn.Accounts.User
  alias Inn.Accounts.Role
  @moduledoc false

  schema "users" do
    field(:name, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    field(:username, :string)
    field(:is_admin, :boolean, virtual: true)

    many_to_many :roles, Role,
      join_through: "users_roles",
      on_delete: :delete_all,
      on_replace: :delete

    timestamps()
  end

  @doc false
  def registration_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:name, :username, :password])
    |> validate_required([:name, :username, :password])
    |> validate_length(:username, min: 3, max: 10)
    |> validate_length(:password, min: 5, max: 10)
    |> unique_constraint(:username)
    |> put_password_hash()
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))

      _ ->
        changeset
    end
  end
end

defmodule Inn.Core.InnService do
  import Ecto.Query
  import Inn.Core.Checker

  alias Inn.Repo
  alias Inn.Core.Inn

  def get_list() do
    from(i in Inn,
      select: %{
        inserted_at: i.inserted_at,
        value: i.value,
        id: i.id,
        check_result: i.check_result
      },
      order_by: [desc: i.inserted_at]
    )
    |> Repo.all()
  end

  def create_new(inn_value, user_ip) do
    changeset =
      Inn.changeset(%Inn{}, %{
        value: inn_value,
        user_ip: user_ip,
        check_result: check_inn(inn_value)
      })

    case Repo.insert(changeset) do
      {:ok, inn} ->
        {:ok, inn}

      {:error, _reason} ->
        {:error, %{errors: changeset}}
    end
  end
end

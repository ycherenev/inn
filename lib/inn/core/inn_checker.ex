defmodule Inn.Core.Checker do
  require Logger

  def check_inn(input_data) do
    Logger.info("check inn: [#{input_data}]")

    case check_data(input_data) do
      10 -> check_10(input_data)
      12 -> check_12(input_data)
      _ -> false
    end
  end

  defp check_data(input_data) do
    case Regex.match?(~r/^\d+$/, input_data) do
      true -> String.length(input_data)
      _ -> false
    end
  end

  defp check_10(numbers) do
    Logger.info("check_10 [#{numbers}]")

    {args, check_number} = get_check_number(numbers, [2, 4, 10, 3, 5, 9, 4, 6, 8])
    {_, [control_number]} = Enum.split(args, 9)
    check_number == control_number
  end

  defp check_12(numbers) do
    Logger.info("check_12 [#{numbers}]")

    {args, check_number_1} = get_check_number(numbers, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8, 0])

    {_, check_number_2} = get_check_number(numbers, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8, 0])

    {_, [control_number1, control_number2]} = Enum.split(args, 10)

    check_number_1 == control_number1 && check_number_2 == control_number2
  end

  defp get_check_number(numbers, multipliers) do
    args =
      numbers
      |> String.codepoints()
      |> Enum.map(&String.to_integer/1)

    sum =
      args
      |> Enum.zip(multipliers)
      |> Enum.map(fn {x, y} -> x * y end)
      |> Enum.sum()

    control_number = rem(sum, 11)

    case control_number > 9 do
      false -> {args, control_number}
      _ -> {args, rem(control_number, 10)}
    end
  end
end

defmodule Inn.Core.Inn do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Jason.Encoder, only: [:id, :inserted_at, :value, :check_result]}

  schema "inn" do
    field :user_ip, :string
    field :value, :string
    field :check_result, :boolean

    timestamps()
  end

  @doc false
  def changeset(inn, attrs) do
    inn
    |> cast(attrs, [:value, :user_ip, :check_result])
    |> validate_required([:value])
  end
end

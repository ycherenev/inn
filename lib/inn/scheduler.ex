defmodule Inn.Scheduler do
  use Quantum.Scheduler,
    timezone: "Europe/Moscow",
    otp_app: :inn
end

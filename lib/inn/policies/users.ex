defmodule Inn.Policies.Users do
  @behaviour Bodyguard.Policy

  def authorize(:block_ip, user, _params) do
    cond do
      user.is_admin -> :ok
      true -> :error
    end
  end
end

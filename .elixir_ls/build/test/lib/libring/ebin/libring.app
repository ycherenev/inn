{application,libring,
             [{applications,[kernel,stdlib,elixir,crypto]},
              {description,"A fast consistent hash ring implementation in Elixir"},
              {modules,['Elixir.HashRing','Elixir.HashRing.App',
                        'Elixir.HashRing.Managed','Elixir.HashRing.Utils',
                        'Elixir.HashRing.Worker','Elixir.Inspect.HashRing']},
              {registered,[]},
              {vsn,"1.4.0"},
              {mod,{'Elixir.HashRing.App',[]}}]}.

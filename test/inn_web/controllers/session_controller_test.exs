defmodule InnWeb.SessionControllerTest do
  use InnWeb.ConnCase

  test "invalid password", %{conn: conn} do
    conn =
      post(
        conn,
        Routes.session_path(conn, :create, %{
          session: %{username: "Operator", password: "pass"}
        })
      )

    response =
      conn
      |> html_response(200)

    assert response =~ "Incorrect username or password"
  end
end

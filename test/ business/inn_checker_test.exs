defmodule Inn.Business.InnCheckerTest do
  use ExUnit.Case
  alias Inn.Core.Checker

  test "check incorrect data" do
    assert false == Checker.check_inn("123")
    assert false == Checker.check_inn("1qwerwer")
  end

  test "check correct data && correct inn" do
    assert true == Checker.check_inn("500100732259") # 12 digits
    assert true == Checker.check_inn("7830002293") #10 digits
  end

  test "check correct data && incorrect inn" do
    assert false == Checker.check_inn("111222333444") # 12 digits
    assert false == Checker.check_inn("123412345") #10 digits
  end

end
defmodule Inn.Repo.Migrations.UpdateInnTable do
  use Ecto.Migration

  def change do
    alter table(:inn) do
      add :check_result, :boolean, default: false
    end
  end
end

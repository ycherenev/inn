defmodule Inn.Repo.Migrations.CreateInn do
  use Ecto.Migration

  def change do
    create table(:inn) do
      add :value, :string
      add :user_ip, :string

      timestamps()
    end

  end
end

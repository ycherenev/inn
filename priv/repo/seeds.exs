# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Inn.Repo.insert!(%Inn.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
import Ecto.Query

alias Inn.Accounts.Role
alias Inn.Accounts.User
alias Inn.Repo

Inn.Repo.insert!(%Role{
  name: "Admin"
})

Inn.Repo.insert!(%Role{
  name: "Operator"
})

admin_role = Repo.one!(from r in Role, where: r.name == "Admin")
operator_role = Repo.one!(from r in Role, where: r.name == "Operator")

Inn.Repo.insert!(%User{
  name: "Admin",
  username: "Admin",
  roles: [admin_role, operator_role],
  # password1
  password_hash: "$2b$12$QzWgBjQeCBQYAVc6FN5xFuINfMYebFmeg4gH8S6/br97FmbagBmem"
})

Inn.Repo.insert!(%User{
  name: "Operator",
  username: "Operator",
  roles: [operator_role],
  # password1
  password_hash: "$2b$12$QzWgBjQeCBQYAVc6FN5xFuINfMYebFmeg4gH8S6/br97FmbagBmem"
})
